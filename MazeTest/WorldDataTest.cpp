/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "WorldData.h"

#include "gtest/gtest.h"

TEST(WorldDataTest, EmptyDataIsInvalid) {
	WorldData data;
	ASSERT_FALSE(data.valid());
}

TEST(WorldDataTest, RectangularDataIsValid) {
	WorldData data;
	std::string row1(".S");
	data.addRow(row1);
	std::string row2("FX");
	data.addRow(row2);
	ASSERT_TRUE(data.valid());
}

TEST(WorldDataTest, IrregularShapeInvalid) {
	WorldData data;
	std::string row1(".S.");
	data.addRow(row1);
	std::string row2("FX");
	data.addRow(row2);
	ASSERT_FALSE(data.valid());
}

TEST(WorldDataTest, ValidWithBothStartAndFinish) {
	WorldData data;
	std::string row1("FS");
	data.addRow(row1);
	ASSERT_TRUE(data.valid());
}

TEST(WorldDataTest, InvalidIfDoesNotHaveStart) {
	WorldData data;
	std::string row1(".X");
	data.addRow(row1);
	std::string row2("FX");
	data.addRow(row2);
	ASSERT_FALSE(data.valid());
}

TEST(WorldDataTest, InvalidIfDoesNotHaveFinish) {
	WorldData data;
	std::string row1(".X");
	data.addRow(row1);
	std::string row2("XS");
	data.addRow(row2);
	ASSERT_FALSE(data.valid());
}

TEST(WorldDataTest, InvalidIfHasMultipleStarts) {
	WorldData data;
	std::string row1(".S");
	data.addRow(row1);
	std::string row2("FS");
	data.addRow(row2);
	ASSERT_FALSE(data.valid());
}

TEST(WorldDataTest, InvalidIfHasMultipleFinishes) {
	WorldData data;
	std::string row1(".S");
	data.addRow(row1);
	std::string row2("FF");
	data.addRow(row2);
	ASSERT_FALSE(data.valid());
}

