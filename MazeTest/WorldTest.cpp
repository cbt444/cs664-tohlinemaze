/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "World.h"

#include "gtest/gtest.h"

class WorldTest: public ::testing::Test {
protected:
	std::stringstream ss;
	World* world;

	WorldTest()
		: ss (std::stringstream::in | std::stringstream::out)
		, world(NULL) {
	}

	~WorldTest() {
		World::destroyWorld(world);
	}
};

TEST_F(WorldTest, MazeCreationAndDeletion) {
	ss <<	".S\n" \
			"FX\n" ;
	world = World::createWorld(ss);
	ASSERT_TRUE(true);
}

TEST_F(WorldTest, CreationFailsForInvalidCharacter) {
	ss <<	".S\n" \
			"Fa\n" ;
	world = World::createWorld(ss);
	ASSERT_EQ(NULL, world);
}

TEST_F(WorldTest, SizeAccessors) {
	ss <<	"XS.F\n";
	world = World::createWorld(ss);
	ASSERT_EQ(1, world->getHeight());
	ASSERT_EQ(4, world->getWidth());
}

TEST_F(WorldTest, SizeAccessorsAlt) {
	ss <<	".S\n" \
			"FX\n" ;
	world = World::createWorld(ss);
	ASSERT_EQ(2, world->getHeight());
	ASSERT_EQ(2, world->getWidth());
}

TEST_F(WorldTest, CellAccessors) {
	ss <<	"XS.F\n";
	world = World::createWorld(ss);
	ASSERT_EQ(World::WALL, world->getCell(0,0));
	ASSERT_EQ(World::START, world->getCell(0,1));
}

TEST_F(WorldTest, CellAccessorsAlt) {
	ss <<	".XS\n" \
			".X.\n" \
			"F..\n" ;
	world = World::createWorld(ss);
	ASSERT_EQ(World::EMPTY, world->getCell(0,0));
	ASSERT_EQ(World::WALL, world->getCell(0,1));
	ASSERT_EQ(World::EMPTY, world->getCell(2,2));
}

TEST_F(WorldTest, SimpleWorldEquality) {
	ss << "SF\n";
	world = World::createWorld(ss);
	std::stringstream ss2(std::stringstream::in | std::stringstream::out);
	ss2 << "SF\n";
	World* world2 = World::createWorld(ss2);

	ASSERT_TRUE(world->equals(*world2));
	World::destroyWorld(world2);
}

TEST_F(WorldTest, SimpleReversedWorldInequality) {
	ss << "SF\n";
	world = World::createWorld(ss);
	std::stringstream ss2(std::stringstream::in | std::stringstream::out);
	ss2 << "FS\n";
	World* world2 = World::createWorld(ss2);

	ASSERT_FALSE(world->equals(*world2));
	World::destroyWorld(world2);
}

TEST_F(WorldTest, WorldWithEmptyOrStepConsideredEquality) {
	ss << "S..F\n";
	world = World::createWorld(ss);
	std::stringstream ss2(std::stringstream::in | std::stringstream::out);
	ss2 << "SooF\n";
	World* world2 = World::createWorld(ss2);

	ASSERT_TRUE(world->equals(*world2));
	World::destroyWorld(world2);
}

TEST_F(WorldTest, StartPositionAccessor) {
	ss <<	".S\n" \
			"FX\n" ;
	world = World::createWorld(ss);
	std::pair<size_t,size_t> expectedStartPosition(0,1);
	ASSERT_EQ(expectedStartPosition, world->getStartPosition());
}

TEST_F(WorldTest, FinishPositionAccessor) {
	ss <<	".S\n" \
			"FX\n" ;
	world = World::createWorld(ss);
	std::pair<size_t,size_t> expectedFinishPosition(1,0);
	ASSERT_EQ(expectedFinishPosition, world->getFinishPosition());
}
