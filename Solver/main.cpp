/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include <fstream>
#include <iostream>

#include "AlgorithmBestFirstSearch.h"
#include "AlgorithmListBasedSearch.h"
#include "AlgorithmRandomStep.h"
#include "World.h"

static void showUsage();

int main(int argc, char* argv[] ) {
	std::cout << "Maze Solver, by Chris Tohline" << std::endl;
	std::cout << "-----------------------------" << std::endl;

	// 2 arguments are required for now: the Maze to solve and the algorithm
	if (argc != 3) {
		showUsage();
		return -1;
	}
	char* fileName = argv[1];
	std::ifstream file(fileName);
	if (file.good()) {
		std::cout << "Reading maze from " << fileName << "..." << std::endl;
		World* world = World::createWorld(file);
		World* origWorld = World::copyWorld(*world);
		if (0 != world) {
			char* algorithmName = argv[2];
			Algorithm* algorithm = 0;
			if (0 == strcmp(algorithmName,"random")) {
				algorithm = new AlgorithmRandomStep(world);
			} else if (0 == strcmp(algorithmName, "depth")) {
				algorithm = new AlgorithmListBasedSearch(world, AlgorithmListBasedSearch::DEPTH);
			} else if (0 == strcmp(algorithmName, "breadth")) {
				algorithm = new AlgorithmListBasedSearch(world, AlgorithmListBasedSearch::BREADTH);
			} else if (0 == strcmp(algorithmName, "best")) {
				algorithm = new AlgorithmBestFirstSearch(world);
			}
			if (0 == algorithm) {
				std::cout << "INVALID ALGORITHM: " << algorithmName << ". Exiting!" << std::endl;
				return -1;
			}
			size_t numSteps = algorithm->run();
			// verify world not changed by solver
			if (!world->equals(*origWorld)) {
				std::cerr << "ALGORITHM CHANGED MAZE! Exiting!" << std::endl;
				return -1;
			}
			if (0 == numSteps) {
				std::cout << "Solver failed!" << std::endl;
			} else {
				std::cout << "Solved in " << numSteps << " steps:" << std::endl;
			}
			world->print();
			delete algorithm;
			World::destroyWorld(world);
			World::destroyWorld(origWorld);
		}
	} else {
		std::cout << "FAILED TO LOAD " << fileName << ". Exiting!" << std::endl;
		return -1;
	}

	return 0;
}

void showUsage() {
	std::cerr << "Expected usage: Solver <maze filename> <algorithm>" << std::endl;
	std::cerr << "Where <algorithm> is one of:" << std::endl;
	std::cerr << "   best" << std::endl;
	std::cerr << "   breadth" << std::endl;
	std::cerr << "   depth" << std::endl;
	std::cerr << "   random" << std::endl;
	std::cerr << "Exiting!" << std::endl;
}
