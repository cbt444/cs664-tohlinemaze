/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include <istream>
#include <utility>
#include <vector>

class WorldData;

class World
{
protected:
	World(size_t height, size_t width);

public:
	// factory method
	static World* createWorld(std::istream& in);
	static void destroyWorld(World* world);
	static World* copyWorld(const World& world);

	void print() const;

	size_t getHeight() const {
		return height;
	}

	size_t getWidth() const {
		return width;
	}

	const std::pair<size_t,size_t>& getStartPosition() const {
		return startPos;
	}

	const std::pair<size_t,size_t>& getFinishPosition() const {
		return finishPos;
	}

	enum MazeValue {
		EMPTY,
		WALL,
		START,
		FINISH,
		STEP
	};

	void setCell(size_t row, size_t col, MazeValue val) {
		if (START == val) {
			startPos.first = row;
			startPos.second = col;
		} else if (FINISH == val) {
			finishPos.first = row;
			finishPos.second = col;
		}
		maze.at(row).at(col) = val;
	}

	MazeValue getCell(size_t row, size_t col) const {
		return maze.at(row).at(col);
	}

	bool equals(const World& otherWorld);

private:
	size_t height, width;
	std::vector< std::vector<MazeValue> > maze;
	std::pair<size_t,size_t> startPos, finishPos;
};

