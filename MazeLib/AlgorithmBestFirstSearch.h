/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include "Algorithm.h"
#include "World.h"

#include <list>
#include <cmath>

class AlgorithmBestFirstSearch : public Algorithm
{
public:
	AlgorithmBestFirstSearch(World* world);
	size_t run();

private:
	struct PosData {
		std::pair<size_t,size_t> position;
		float distance;

		PosData(const std::pair<size_t,size_t>& pos, float dist)
			: position(pos), distance(dist) {
		}

		static bool compare(const PosData& left, const PosData& right) {
			return left.distance < right.distance;
		}

		bool operator==(const PosData &other) const {
			return position == other.position && distance == other.distance;
		}

		void calcDist(const std::pair<size_t,size_t>& otherPosition) {
			float val1 = static_cast<float>(otherPosition.first) - static_cast<float>(position.first);
			val1 *= val1;
			float val2 = static_cast<float>(otherPosition.second) - static_cast<float>(position.second);
			val2 *= val2;
			distance = sqrt(val1+val2);
		}
	};

	bool checkCell(size_t row, size_t col, std::list< PosData > &addList, const std::pair<size_t,size_t>& currentPosition);
	void addToOpenListIfNotAlreadyThere(std::list< PosData > &openList, std::list< PosData >& addList);
};
