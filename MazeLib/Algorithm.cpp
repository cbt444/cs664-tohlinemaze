/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "Algorithm.h"

#include <time.h>
#include <iostream>

Algorithm::Algorithm(World* theWorld) : world(theWorld) {
	std::srand(static_cast<unsigned int>(time(0)));

	extraMazeData.resize(world->getHeight());
	std::vector< std::vector<ExtraMazeData> >::iterator endItr = extraMazeData.end();
	std::vector< std::vector<ExtraMazeData> >::iterator itr = extraMazeData.begin();
	for(; itr != endItr; ++itr) {
		itr->resize(world->getWidth());
	}
}

void Algorithm::initializeExtraDataForStartPosition() {
	const std::pair<size_t,size_t>& startPos = world->getStartPosition();
	ExtraMazeData& extraData = extraMazeData[startPos.first][startPos.second];
	extraData.bestPrevious = startPos;
	extraData.numSteps = 0;
}


void Algorithm::clearMaze() {
	for(size_t i=0; i<world->getHeight(); ++i) {
		for(size_t j=0; j<world->getWidth(); ++j) {
			if (World::STEP == world->getCell(i,j)) {
				world->setCell(i,j,World::EMPTY);
			}
		}
	}
}

void Algorithm::applyBacktrackFromPosition(std::pair<size_t,size_t> position) {
	std::cout << "(" << extraMazeData[position.first][position.second].numSteps+1 << " length) ";
	while(World::EMPTY == world->getCell(position.first, position.second)) {
		world->setCell(position.first, position.second, World::STEP);
		position = extraMazeData[position.first][position.second].bestPrevious;
	}
}

void Algorithm::updateFewestStepsToPosition(const std::pair<size_t,size_t>& currentPosition, const std::pair<size_t,size_t>& nextPosition) {
	ExtraMazeData& valExtraData = extraMazeData[nextPosition.first][nextPosition.second];
	ExtraMazeData& curExtraData = extraMazeData[currentPosition.first][currentPosition.second];
	if (valExtraData.numSteps > curExtraData.numSteps+1) {
		valExtraData.bestPrevious = currentPosition;
		valExtraData.numSteps = curExtraData.numSteps+1;
	}
}