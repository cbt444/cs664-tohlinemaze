/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "WorldData.h"

#include <algorithm>
#include <iostream>

WorldData::WorldData() {
}

bool WorldData::valid() {
	return notEmpty()
		&& isRectangular()
		&& hasOneAndOnlyOneStart()
		&& hasOneAndOnlyOneFinish();
}

bool WorldData::notEmpty() {
	bool result = 0 != data.size();
	if (!result) {
		std::cerr << "Maze data is empty!" << std::endl;
	}
	return result;
}
bool WorldData::isRectangular() {
	const size_t rowSize = data.begin()->size();
	std::vector<std::string>::iterator endItr = data.end();
	for(std::vector<std::string>::iterator itr = data.begin(); itr != endItr; ++itr) {
		if (itr->size() != rowSize) {
			std::cerr << "Maze data is not rectangular!" << std::endl;
			return false;
		}
	}
	return true;
}

bool WorldData::hasOneAndOnlyOneStart() {
	size_t sCount = countLetter('S');
	bool result = 1 == sCount;
	if (!result) {
		if (0 == sCount) {
			std::cerr << "Maze data has no start position!" << std::endl;
		} else {
			std::cerr << "Maze data has multiple start positions!" << std::endl;
		}
	}
	return result;
}

bool WorldData::hasOneAndOnlyOneFinish() {
	size_t fCount = countLetter('F');
	bool result = 1 == fCount;
	if (!result) {
		if (0 == fCount) {
			std::cerr << "Maze data has no finish position!" << std::endl;
		} else {
			std::cerr << "Maze data has multiple finish positions!" << std::endl;
		}
	}
	return result;
}

size_t WorldData::countLetter(char letter) {
	size_t count = 0;
	std::vector<std::string>::iterator endItr = data.end();
	for(std::vector<std::string>::iterator itr = data.begin(); itr != endItr; ++itr) {
		count += std::count(itr->begin(), itr->end(), letter);
	}
	return count;
}