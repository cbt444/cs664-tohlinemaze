/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include <string>
#include <vector>

class WorldData
{
	std::vector<std::string> data;
public:
	WorldData();

	// populate data
	void addRow(const std::string& row) {
		data.push_back(row);
	}

	// does the current data comprise a valid maze?
	bool valid();

	// retrieve data
	const std::string& getRow(size_t row) const {
		return data.at(row);
	}

	const size_t getNumRows() const {
		return data.size();
	}


private:
	bool notEmpty();
	bool isRectangular();
	bool hasOneAndOnlyOneStart();
	bool hasOneAndOnlyOneFinish();

	size_t countLetter(char letter);
};

