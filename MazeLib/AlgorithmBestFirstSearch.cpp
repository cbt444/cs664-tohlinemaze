/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "AlgorithmBestFirstSearch.h"

#include <algorithm>

AlgorithmBestFirstSearch::AlgorithmBestFirstSearch(World* world) : Algorithm(world) {
}


size_t AlgorithmBestFirstSearch::run() {
	PosData currentCell(std::pair<size_t,size_t>(0,0), 0);
	std::list< PosData > openList;
	currentCell.position = world->getStartPosition();
	currentCell.calcDist(world->getFinishPosition());
	initializeExtraDataForStartPosition();
	for(size_t step=1; step<1000000; ++step) {
		std::list< PosData > addList;
		// UP
		if (currentCell.position.first >0 && checkCell(currentCell.position.first-1, currentCell.position.second, addList, currentCell.position)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell.position);
			return step;
		}

		// DOWN
		if (currentCell.position.first < world->getHeight()-1 && checkCell(currentCell.position.first+1, currentCell.position.second, addList, currentCell.position)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell.position);
			return step;
		}

		// LEFT
		if (currentCell.position.second >0 && checkCell(currentCell.position.first, currentCell.position.second-1, addList, currentCell.position)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell.position);
			return step;
		}

		// RIGHT
		if (currentCell.position.second < world->getWidth()-1 && checkCell(currentCell.position.first, currentCell.position.second+1, addList, currentCell.position)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell.position);
			return step;
		}

		// add items from addList to open list
		addToOpenListIfNotAlreadyThere(openList, addList);


		if (0 == openList.size()) {
			// give up
			return 0;
		}

		openList.sort(PosData::compare);
		currentCell = openList.front();
		openList.pop_front();
		world->setCell(currentCell.position.first, currentCell.position.second, World::STEP);
	}
	return 0;
}

bool AlgorithmBestFirstSearch::checkCell(size_t row, size_t col, std::list< PosData >& addList, const std::pair<size_t,size_t>& currentPosition)
{
	bool complete = false;
	World::MazeValue val = world->getCell(row, col);
	if (World::FINISH == val) {
		complete = true;
	} else if (World::EMPTY == val) {
		PosData val(std::pair<size_t,size_t>(row, col), 0);
		val.calcDist(world->getFinishPosition());
		addList.push_back(val);
		updateFewestStepsToPosition(currentPosition, val.position);
	}
	return complete;
}

void AlgorithmBestFirstSearch::addToOpenListIfNotAlreadyThere(std::list< PosData >& openList, std::list< PosData >& addList) {
	while(addList.size() > 0) {
		PosData& val = addList.back();
		std::list< PosData >::iterator itr = std::find(openList.begin(), openList.end(), val);
		if (openList.end() == itr) {
			openList.push_back(val);
		}
		addList.pop_back();
	}
}
