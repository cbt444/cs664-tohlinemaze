/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include "World.h"
#include <vector>

class Algorithm {
public:
	Algorithm(World* world);
	virtual ~Algorithm() {}

public:
	// returns number of steps
	virtual size_t run() =0;

protected:
	World* world;

	struct ExtraMazeData {
		std::pair<size_t,size_t> bestPrevious;
		size_t numSteps;

		ExtraMazeData() {
			numSteps = 1000000;
		}
	};
	std::vector< std::vector<ExtraMazeData> > extraMazeData;

	void initializeExtraDataForStartPosition();
	void clearMaze();
	void applyBacktrackFromPosition(std::pair<size_t,size_t> position);
	void updateFewestStepsToPosition(const std::pair<size_t,size_t>& currentPosition, const std::pair<size_t,size_t>& nextPosition);
};
