/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include "Algorithm.h"
#include "World.h"

#include <list>
#include <vector>

class AlgorithmListBasedSearch : public Algorithm
{
public:
	enum Heuristic {
		BREADTH, DEPTH
	};
	AlgorithmListBasedSearch(World* world, Heuristic searchType);
	size_t run();

private:
	bool checkCell(size_t row, size_t col, std::vector< std::pair<size_t,size_t> > &addList, const std::pair<size_t,size_t>& currentPosition);
	void addToOpenListIfNotAlreadyThere(std::list< std::pair<size_t,size_t> > &openList, std::vector< std::pair<size_t,size_t> >& addList);

	Heuristic searchType;
};
