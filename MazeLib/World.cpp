/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "World.h"
#include "WorldData.h"

#include <iterator>
#include <iostream>

static void readLines(WorldData &data, std::istream& in) {
	std::istreambuf_iterator<char> eos; // end-of-range iterator
	std::istreambuf_iterator<char> itr (in.rdbuf()); // stdin iterator
	while(itr!=eos) {
		std::string mystring;
		while (itr!=eos && *itr!='\n') mystring+=*itr++;
		while (itr!=eos && *itr == '\n') ++itr;
		data.addRow(mystring);
	}
}

//static
World* World::createWorld(std::istream& in) {
	WorldData data;
	readLines(data, in);
	if (data.valid()) {
		size_t height = data.getNumRows();
		size_t width = data.getRow(0).size();
		World* world = new World(height, width);
		bool valid = true;
		for(size_t i=0; valid && i<height; ++i) {
			const std::string& row = data.getRow(i);
			std::string::const_iterator endItr = row.end();
			size_t j=0;
			MazeValue val = EMPTY;;
			for(std::string::const_iterator itr = row.begin(); valid && itr != endItr; ++itr, ++j) {
				const char& ch = *itr;
				switch(ch) {
				case 'X':
					val = WALL;
					break;
				case '.':
					val = EMPTY;
					break;
				case 'S':
					val = START;
					break;
				case 'F':
					val = FINISH;
					break;
				case 'o':
					val = STEP;
					break;
				default:
					valid = false;
					std::cerr << "Maze data contains invalid character:" << ch << std::endl;
					break;
				}
				if (valid) {
					world->setCell(i, j, val);
				}
			}
		}
		if (valid) {
			return world;
		} else {
			delete world;
		}
	}
	return 0;
}

//static
void World::destroyWorld(World* world) {
	delete world;
}

//static
World* World::copyWorld(const World& sourceWorld) {
	World* worldCopy = new World(sourceWorld.getHeight(), sourceWorld.getWidth());
	for(size_t i=0; i<sourceWorld.getHeight(); ++i) {
		for(size_t j=0; j<sourceWorld.getWidth(); ++j) {
			worldCopy->setCell(i,j,sourceWorld.getCell(i,j));
		}
	}
	return worldCopy;
}

bool World::equals(const World& otherWorld) {
	if (height != otherWorld.height || width != otherWorld.width) {
		return false;
	}
	for(size_t i=0; i<otherWorld.getHeight(); ++i) {
		for(size_t j=0; j<otherWorld.getWidth(); ++j) {
			MazeValue first = getCell(i,j);
			MazeValue second = otherWorld.getCell(i,j);
			if (first == STEP) {
				first = EMPTY;
			}
			if (second == STEP) {
				second = EMPTY;
			}
			if (first != second) {
				return false;
			}
		}
	}
	return true;
}

World::World(size_t theHeight, size_t theWidth) : height(theHeight), width(theWidth) {
	maze.resize(height);
	std::vector< std::vector<MazeValue> >::iterator endItr = maze.end();
	std::vector< std::vector<MazeValue> >::iterator itr = maze.begin();
	for(; itr != endItr; ++itr) {
		itr->resize(width);
	}
}

void outputLine(size_t len) {
	for(size_t i=0; i<len; ++i) {
		std::cout << "-";
	}
	std::cout << std::endl;
}

void World::print() const {
	outputLine(width*3+2);
	std::vector< std::vector<MazeValue> >::const_iterator rowEndItr = maze.end();
	std::vector< std::vector<MazeValue> >::const_iterator rowItr = maze.begin();
	for(; rowItr != rowEndItr; ++rowItr) {
		std::cout << "|";
		std::vector<MazeValue>::const_iterator colEndItr = rowItr->end();
		std::vector<MazeValue>::const_iterator colItr = rowItr->begin();
		for(; colItr != colEndItr; ++colItr) {
			switch(*colItr) {
			case EMPTY:
				std::cout << "   ";
				break;
			case WALL:
				std::cout << "XXX";
				break;
			case START:
				std::cout << "[S]";
				break;
			case FINISH:
				std::cout << "[F]";
				break;
			case STEP:
				std::cout << "...";
				break;
			}
		}
		std::cout << "|" << std::endl;
	}
	outputLine(width*3+2);
}
