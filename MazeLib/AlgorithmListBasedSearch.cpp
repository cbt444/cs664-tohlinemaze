/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "AlgorithmListBasedSearch.h"

#include <algorithm>

AlgorithmListBasedSearch::AlgorithmListBasedSearch(World* world, Heuristic theSearchType) : Algorithm(world), searchType(theSearchType) {
}

size_t AlgorithmListBasedSearch::run() {
	std::pair<size_t,size_t> currentCell(0,0);
	std::list< std::pair<size_t,size_t> > openList;
	currentCell = world->getStartPosition();
	initializeExtraDataForStartPosition();
	for(size_t step=1; step<1000000; ++step) {
		std::vector< std::pair<size_t,size_t> > addList;
		// UP
		if (currentCell.first >0 && checkCell(currentCell.first-1, currentCell.second, addList, currentCell)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell);
			return step;
		}

		// DOWN
		if (currentCell.first < world->getHeight()-1 && checkCell(currentCell.first+1, currentCell.second, addList, currentCell)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell);
			return step;
		}

		// LEFT
		if (currentCell.second >0 && checkCell(currentCell.first, currentCell.second-1, addList, currentCell)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell);
			return step;
		}

		// RIGHT
		if (currentCell.second < world->getWidth()-1 && checkCell(currentCell.first, currentCell.second+1, addList, currentCell)) {
			clearMaze();
			applyBacktrackFromPosition(currentCell);
			return step;
		}

		// add items from addList to open list
		addToOpenListIfNotAlreadyThere(openList, addList);


		if (0 == openList.size()) {
			// give up
			return 0;
		}

		if (BREADTH == searchType) {
			currentCell = openList.front();
			openList.pop_front();
		} else if (DEPTH == searchType) {
			currentCell = openList.back();
			openList.pop_back();
		}
		world->setCell(currentCell.first, currentCell.second, World::STEP);
	}
	return 0;
}

bool AlgorithmListBasedSearch::checkCell(size_t row, size_t col, std::vector< std::pair<size_t,size_t> >& addList, const std::pair<size_t,size_t>& currentPosition)
{
	bool complete = false;
	World::MazeValue val = world->getCell(row, col);
	if (World::FINISH == val) {
		complete = true;
	} else if (World::EMPTY == val) {
		std::pair<size_t,size_t> val(row, col);
		addList.push_back(val);
		updateFewestStepsToPosition(currentPosition, val);
	}
	return complete;
}

void AlgorithmListBasedSearch::addToOpenListIfNotAlreadyThere(std::list< std::pair<size_t,size_t> >& openList, std::vector< std::pair<size_t,size_t> >& addList) {
	std::random_shuffle(addList.begin(), addList.end());
	while(addList.size() > 0) {
		std::pair<size_t,size_t>& val = addList.back();
		std::list< std::pair<size_t,size_t> >::iterator itr = std::find(openList.begin(), openList.end(), val);
		if (openList.end() == itr) {
			openList.push_back(val);
		}
		addList.pop_back();
	}
}
