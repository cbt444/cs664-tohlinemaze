/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#include "AlgorithmRandomStep.h"

#include <vector>

AlgorithmRandomStep::AlgorithmRandomStep(World* world) : Algorithm(world) {
}

size_t AlgorithmRandomStep::run() {
	std::pair<size_t,size_t> currentCell(0,0);
	currentCell = world->getStartPosition();
	for(size_t step=1; step<10000000; ++step) {
		std::vector< std::pair<size_t,size_t> > possibilities;
		// UP
		if (currentCell.first >0) {
			World::MazeValue val = world->getCell(currentCell.first-1, currentCell.second);
			if (World::FINISH == val) {
				// done!
				return step;
			}
			if (World::EMPTY == val || World::STEP == val) {
				possibilities.push_back(std::pair<size_t,size_t>(currentCell.first-1, currentCell.second));
			}
		}

		// DOWN
		if (currentCell.first < world->getHeight()-1) {
			World::MazeValue val = world->getCell(currentCell.first+1, currentCell.second);
			if (World::FINISH == val) {
				// done!
				return step;
			}
			if (World::EMPTY == val || World::STEP == val) {
				possibilities.push_back(std::pair<size_t,size_t>(currentCell.first+1, currentCell.second));
			}
		}

		// LEFT
		if (currentCell.second >0) {
			World::MazeValue val = world->getCell(currentCell.first, currentCell.second-1);
			if (World::FINISH == val) {
				// done!
				return step;
			}
			if (World::EMPTY == val || World::STEP == val) {
				possibilities.push_back(std::pair<size_t,size_t>(currentCell.first, currentCell.second-1));
			}
		}

		// RIGHT
		if (currentCell.second < world->getWidth()-1) {
			World::MazeValue val = world->getCell(currentCell.first, currentCell.second+1);
			if (World::FINISH == val) {
				// done!
				return step;
			}
			if (World::EMPTY == val || World::STEP == val) {
				possibilities.push_back(std::pair<size_t,size_t>(currentCell.first, currentCell.second+1));
			}
		}

		currentCell = possibilities.at(std::rand()%possibilities.size());
		world->setCell(currentCell.first, currentCell.second, World::STEP);
	}
	return 0;
}
