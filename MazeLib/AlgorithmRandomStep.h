/*
 * Chris Tohline (ctohline@bu.edu)
 * CS 664 Artificial Intelligence
 * Final Exam Project - Maze Solver
 */

#pragma once

#include "Algorithm.h"
#include "World.h"

class AlgorithmRandomStep : public Algorithm
{
public:
	AlgorithmRandomStep(World* world);
	size_t run();
};

